package co.com.coomeva.serviceGeneraToken.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import co.com.coomeva.serviceGeneraToken.modelos.profile.Application;
import co.com.coomeva.serviceGeneraToken.modelos.profile.Role;
import co.com.coomeva.serviceGeneraToken.modelos.profile.UserVo;
import co.com.coomeva.serviceGeneraToken.modelos.seguridad.ResponseToken;
import co.com.coomeva.serviceGeneraToken.services.ProfileServiceOldService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
	
	@Value("${nombre.app.profile}")			//Se inyecta desde application.properties
	private String app;						//Nombre de la app a validar en el Profile
	
	@Value("${nombre.rol.profile}")
	private String NOMBRE_AUTORIZADOR_OUTH;
	
	@Autowired
	private ProfileServiceOldService profileService;
	
	@PostMapping(value = "/token", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseToken getToken(@RequestParam("username") String username, @RequestParam("password") String password) throws AccesoDenegadoException {

		UserVo usuario = null;
		boolean esAutorizadorOuth = false;
		//final String NOMBRE_AUTORIZADOR_OUTH = "Autorizador Outh";
		
		ResponseToken responseToken = new ResponseToken();
		
		//Primero se da por hecho que no tiene autorizacion
		responseToken.setCodMensaje("-1");
		responseToken.setMessage("No autorizado");
		responseToken.setToken("");
		
		//Valida en coonectados-auto si es un usuario y app validos
		try {			
			usuario = validarUsuarioApp(username, password, app);
		} catch (JsonMappingException e) {
			throw new AccesoDenegadoException();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//Tag name dentro del tag roles debe retornar "Autorizador Outh"
		for (Application aplicacion : usuario.getApplications()) {
			if (aplicacion.getName().equalsIgnoreCase(app)) {
				for (Role rol : aplicacion.getRoles()) {
					if (rol.getName().equalsIgnoreCase(NOMBRE_AUTORIZADOR_OUTH)) {
						esAutorizadorOuth = true;
					}
				}
			}
		}
		
		if (usuario.getAuthorized() != null && usuario.getAuthorized().equals("true") 
				&& esAutorizadorOuth) {								//Las dos condiciones para poder generar el token
			String token = getJWTToken(username);
			responseToken.setCodMensaje("1");
			responseToken.setMessage("Autorizado");
			responseToken.setToken(token);
		}

		return responseToken;
		
	}
	
	/**
	 * lo comentado no hace falta usarlo ya que como no se esta consumiendo de un servicio aparte solo se deja lo principal
	 * el profileService que se utiliza y como devuelve un userVo ahi agrego lo q encuentre.
	 * 
	 * */
	public UserVo validarUsuarioApp(String username, String password, String app) throws JsonProcessingException, IOException {
	
		UserVo userVo = null;
		
		userVo= profileService.getUserProfile(username, password, app);

		return userVo;
	}

	private String getJWTToken(String username) {
		
		String secretKey = "mySecretKey";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_USER");
		
		String token = Jwts
				.builder()
				.setId("softtekJWT")
				.setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512,
						secretKey.getBytes()).compact();

		return "Bearer " + token;
	}
	
	
	
}
