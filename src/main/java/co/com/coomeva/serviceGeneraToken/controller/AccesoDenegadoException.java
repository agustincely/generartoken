package co.com.coomeva.serviceGeneraToken.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.UNAUTHORIZED, reason="Usuario y/o aplicacion no autorizados")  // 401
public class AccesoDenegadoException extends RuntimeException {

	private static final long serialVersionUID = 6596239645769414111L;

}
