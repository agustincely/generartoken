package co.com.coomeva.serviceGeneraToken.services;

import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.SOAPMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import co.com.coomeva.serviceGeneraToken.modelos.profile.Application;
import co.com.coomeva.serviceGeneraToken.modelos.profile.ParameterSoap;
import co.com.coomeva.serviceGeneraToken.modelos.profile.Role;
import co.com.coomeva.serviceGeneraToken.modelos.profile.Section;
import co.com.coomeva.serviceGeneraToken.modelos.profile.SoapWebServiceController;
import co.com.coomeva.serviceGeneraToken.modelos.profile.UserVo;



@Service
public class ProfileServiceOldService {

	//public static final String DIRECTORY_APP_PROFILE = "2";
	//public static final String VALIDATE_APP_PROFILE = "1";

	@Value("${directory.app.profile}")
	private String DIRECTORY_APP_PROFILE;
	
	@Value("${validate.app.profile}")
	private String VALIDATE_APP_PROFILE;
	
	@Autowired
	Environment env;
	
	@Value("${url.soap.validacion.profile}")				//Se inyecta desde application.properties
	private String urlSoapValidacionProfile;	//Url SOAP validacion del Profile
	

	public UserVo getUserProfile(String userName, String password, String app) {
		JSONObject userVoObj = null;
		UserVo userVo = null;
		boolean userExist=false;

		try{

			userVoObj = consumirProfileManager(userName, password, app, urlSoapValidacionProfile);
			
			userVo = new UserVo();
			
			try {
				userVo.setErrorMessage(userVoObj.getString("faultstring"));
			} catch (Exception e) {
				userExist=true;
			}
			
			if(userExist) {
				try {
					userVo.setEmployeeId(userVoObj.getString("ax21:employeeId"));
				} catch (Exception e) {
					userVo.setEmployeeId("");
				}
				try {
					userVo.setAuthorized(userVoObj.getBoolean("ax21:authorized")+"");
				} catch (Exception e) {
					userVo.setId("");
				}
				try {
					userVo.setId(String.valueOf(userVoObj.getInt("ax21:id")));
				} catch (Exception e) {
					userVo.setId("");
				}
				try {
					userVo.setLock(userVoObj.getString("ax21:lock"));
				} catch (Exception e) {
					userVo.setLock("");
				}
				try {
					userVo.setMail(userVoObj.getString("ax21:mail"));
				} catch (Exception e) {
					userVo.setMail("");
				}
				try {
					userVo.setMail2(userVoObj.getString("ax21:mail2"));
				} catch (Exception e) {
					userVo.setMail2("");
				}
				try {
					userVo.setName(userVoObj.getString("ax21:name"));
				} catch (Exception e) {
					userVo.setName("");
				}
				try {
					userVo.setTelephone(userVoObj.getString("ax21:telephone"));
				} catch (Exception e) {
					userVo.setTelephone("");
				}
				try {
					userVo.setUserId(userVoObj.getString("ax21:userId"));
				} catch (Exception e) {
					userVo.setUserId("");
				}
				try {
					userVo.setUserType(userVoObj.getString("ax21:userType"));
				} catch (Exception e) {
					userVo.setUserType("");
				}

				List<Application> applications = new ArrayList<Application>();  
				try {
					JSONObject applicationsObj = userVoObj.getJSONObject("ax21:applications");
					Application application = new Application();
					application.setName(applicationsObj.getString("ax21:name"));
				
					if(applicationsObj.has("ax21:sections")) {
							JSONArray sectionsObj = applicationsObj.getJSONArray("ax21:sections");
							
							List<Section> sections = new ArrayList<Section>();
							for(int i = 0; i<sectionsObj.length();i++){
								Section section = new Section();
								JSONObject sect = (JSONObject) sectionsObj.get(i);
								section.setName(sect.getString("ax21:name"));
								List<String> actions = new ArrayList<String>();
								try {
									JSONArray actionsObt = sect.getJSONArray("ax21:actions");
									for(int y = 0; y<actionsObt.length(); y++){
										actions.add(actionsObt.get(y).toString());
									}
								} catch (Exception eer) {
									actions.add(sect.getString("ax21:actions"));
								}
								section.setActions(actions);
								sections.add(section);
							}
							application.setSections(sections);
							
					}
					
					if(applicationsObj.has("ax21:roles")) {
						List<Role> roles = new ArrayList<Role>();
						
						try {

							JSONArray rolesObj = applicationsObj.getJSONArray("ax21:roles");
							for(int m = 0; m<rolesObj.length();m++){
								Role role = new Role();
								JSONObject roleObj = (JSONObject) rolesObj.get(m);
								role.setName(roleObj.getString("ax21:name"));
								roles.add(role);
							}
						} catch (Exception etr) {

							JSONObject rolesObj = applicationsObj.getJSONObject("ax21:roles");
							Role role = new Role();
							role.setName(rolesObj.getString("ax21:name"));
							roles.add(role);
						}
						
						application.setRoles(roles);
					
					}		
						applications.add(application);
				
					
				} catch (Exception e) {
					e.printStackTrace();
					JSONObject applicationsObj = userVoObj.getJSONObject("ax21:applications");
					JSONObject sectionsObj = applicationsObj.getJSONObject("ax21:sections");
					JSONArray actionObj = sectionsObj.getJSONArray("ax21:actions");
					Application application = new Application();
					Section section = new Section();
					String sect = sectionsObj.getString("ax21:name");
					section.setName(sect);
					List<Section> sections = new ArrayList<Section>();
					List<String> actions = new ArrayList<String>();
					for(int i = 0; i<actionObj.length();i++){
						actions.add(actionObj.get(i).toString());
						
					}
					section.setActions(actions);
					sections.add(section);
					application.setSections( sections);

					List<Role> roles = new ArrayList<Role>();
					
					try {

						JSONArray rolesObj = applicationsObj.getJSONArray("ax21:roles");
						for(int m = 0; m<rolesObj.length();m++){
							Role role = new Role();
							JSONObject roleObj = (JSONObject) rolesObj.get(m);
							role.setName(roleObj.getString("ax21:name"));
							
							roles.add(role);
						}
					} catch (Exception etr) {

						JSONObject rolesObj = applicationsObj.getJSONObject("ax21:roles");
						Role role = new Role();
						role.setName(rolesObj.getString("ax21:name"));
						
						roles.add(role);
					}
					
					application.setRoles(roles);
					applications.add(application);
				}
				userVo.setApplications(applications);
			}
			
		}catch(

	Exception e)
	{

	}return userVo;
	}

	/**
	 * @Autor: emurilloa
	 * @Fecha: 06/11/2018
	 * @Modificado: efarboleda@latam.stefanini.com
	 * @Desc: Obtener los datos del profile manager
	 * 
	 * @param userName
	 * @param password
	 * 
	 * @return jsonObject
	 */
	private JSONObject consumirProfileManager(String userName, String password, String app, String url) {
		ParameterSoap wsdlData = new ParameterSoap();
		SoapWebServiceController soapWSC = new SoapWebServiceController();
		String directory = DIRECTORY_APP_PROFILE;
		String applicacionBuscada = app;
		String validaAplicacion = VALIDATE_APP_PROFILE;

		wsdlData.addParameterWebService(0, directory);
		wsdlData.addParameterWebService(1, userName);
		wsdlData.addParameterWebService(2, password);
		wsdlData.addParameterWebService(3, validaAplicacion);
		wsdlData.addParameterWebService(4, applicacionBuscada);
		wsdlData.addParameterRequest(0, "directory");
		wsdlData.addParameterRequest(1, "userName");
		wsdlData.addParameterRequest(2, "password");
		wsdlData.addParameterRequest(3, "validaAplicacion");
		wsdlData.addParameterRequest(4, "app");

		wsdlData.setServerUri(url);
		wsdlData.setComplexType("ges:validateUserApp");
		wsdlData.setHeader("SOAPAction");
		wsdlData.setPrefix("ges");
		wsdlData.setTargetNamespace("http://geside.coomeva.com.co");
		wsdlData.setEncodingStyle("NA");

		soapWSC.callWebService(wsdlData);
		SOAPMessage rsXML = soapWSC.getSoapResponse();
		String response = "";
		try {
			response = soapWSC.getStringSOAPResponse(rsXML);
		} catch (Exception e) {

		}
		JSONObject jsonObject = null;
		JSONObject jsonObject2 = null;
		JSONObject jsonBody = null;
		try {
			jsonObject = XML.toJSONObject(response);
			jsonBody = jsonObject.getJSONObject("soapenv:Envelope").getJSONObject("soapenv:Body")
					.getJSONObject("ns:validateUserAppResponse").getJSONObject("ns:return");
		} catch (JSONException e) {

			try {
				jsonObject2 = XML.toJSONObject(response);
				jsonBody = jsonObject2.getJSONObject("soapenv:Envelope").getJSONObject("soapenv:Body")
						.getJSONObject("soapenv:Fault");
			} catch (JSONException e1) {

			}

		}
		return jsonBody;
	}

}
