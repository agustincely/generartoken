package co.com.coomeva.serviceGeneraToken.modelos.seguridad;

import java.io.Serializable;

public class ResponseToken implements Serializable {

	private static final long serialVersionUID = -7668425395658264204L;
	
	private String codMensaje;

	private String message;

	private String token;

	public String getCodMensaje() {
		return codMensaje;
	}

	public void setCodMensaje(String codMensaje) {
		this.codMensaje = codMensaje;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		return "ResponseToken [codMensaje=" + codMensaje + ", message=" + message + ", token=" + token + "]";
	}

	
}